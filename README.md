LAB ASSIGNMENT # 1
We will take the example of a library and model it in our program using Object Oriented programming. As you have learnt about classes and objects, read the case study to identify: a) Number of classes required b) Number of objects required c) Attributes of each class d) Functions required to access/alter attributes of objects

A library has many books. At any point in time, the total number of books present in the library is known. Whenever someone borrows a book, the number of books present in the library is decreased by one, and the number of books borrowed goes up by one. At all times, the number of books present and borrowed should be up to date.  
Every book present in the library has the following attributes: ‘title’, ‘author’, ‘publisher’ and ‘is_available’. Note that the ‘is_available’ attribute represents whether the book is available for borrowing or not.
A member can only borrow one book at a time. The following details of a member are known: his/her name, phone number, the book that he/she currently has.

On 1st March 2016, The Old City Library had 5 books in total and 3 members.

 






On 2nd March 2016:
Tom borrowed ‘The Secret Garden’
Richard borrowed ‘The Cat in the Hat’
Hillary returned ‘The Wind in the Willows’



Answer the following statements for 2nd March 2016, by creating relevant functions to handle borrowing of books, maintaining count of books available and borrowed from the library, as well as books borrowed by members. (Use print statements to display your answers) 
How many books are available at closing time of The Old City Library?
How many books are borrowed at closing time of The Old City Library?
Which customer has borrowed which book?  (e.g. Tom: The Secret Garden)
List of books which are available for borrowing. 
  