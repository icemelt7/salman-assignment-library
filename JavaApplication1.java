/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;
import java.util.*;
/**
 *
 * @author salma
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
     
   public static void main(String args[]) {    
    
    Vehicle vehicleRate = new Vehicle();
    Machine machineRate = new Machine();
    //Vehicle car = new Vehicle();
   // Vehicle bike = new Vehicle();
      // ArrayList<Vehicle> vehicle = new ArrayList();
       //vehicle.add(new Vehicle());
       for (int i = 1; i <=5; i++) {           
           System.out.println("At the end of "+i+". year: ");
           System.out.print("Van rate is: "+vehicleRate.vanRate()+"\n");
           System.out.print("Car rate is: "+vehicleRate.carRate()+"\n");
           System.out.print("Bike rate is: "+vehicleRate.bikeRate()+"\n");
           if(i<=2)
           System.out.print("Machines rate is: "+machineRate.machineRate()+"\n");
           else
               System.out.print("Machines disposed off. Total money of Plant & Machinery is: "+ machineRate.totalMoney+"\n");
           
           System.out.println("---------------------------------------------");
       }
    }
}

class Vehicle {
    private int vanCost = 3*25000;
    private int bikeCost = 4*7000;
    private int carCost = 2*15000;
    public double vanRate(){
        vanCost *=0.93;
        return vanCost;      
    }
    public double bikeRate(){
        bikeCost *=0.97;
        return bikeCost;      
    }
    public double carRate(){
        carCost *=0.95;
        return carCost;      
    }
}

class Machine{
    public int totalMoney = 500000;
    private int machineCost = 17000;
    public double machineRate() {
        this.machineCost *= 0.925;
        this.totalMoney -= machineCost;
        return machineCost;
    }
    
}