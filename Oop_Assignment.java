/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author salma
 */
import java.util.*;

public class Oop_Assignment {

  /**
   * @param args the command line arguments
   */
  static Scanner key = new Scanner(System.in);

  public static void main(String[] args) {
    //Load Library
    Library library = new Library();
    library.LoadBooks(new String[][] { 
        { "Charlotte’s Web", "E.B. White", "Harper & Brothers", "1" },
        { "The Secret Garden", "Frances Hodgson", "Frederick A. Stokes", "1" },
        { "Charlie and the Chocolate Factory", "Roald Dahl", "Puffin Books", "1" },
        { "The Cat in the Hat", "Dr. Seuss", "Random House", "1" },
        { "The Wind in the Willows", "Kenneth Grahame", "Methuen Publishing", "1" } });

    library.LoadMembers(new String[][] { 
        { "Tom", "12345678", "" }, 
        { "Richard", "23456789", "" },
        { "Hillary", "45678901", "The Wind in the Willows" } 
    });

    //Events of 2nd March 2016
    library.BorrowBook("Tom", "The Secret Garden");
    library.BorrowBook("Richard", "The Cat in the Hat");
    library.ReturnBook("Hillary", "The Wind in the Willows");

    //Results after 2nd March
    System.out.println("Number of books Available: " + library.booksAvail().size());
    System.out.println("Number of books Borrowed: " + library.booksBorrowed().size());
    System.out.println("Current Library Status: " + library.GetLibraryStatus());
    System.out.println("Books available for borrowing: " + library.booksAvail()); 
  }
}

class Member {
  public String name;
  public String phone_number;
  public Book current_book;

  public Member(String name, String phone_number, String book_name) {
    this.name = name;
    this.phone_number = phone_number;
  }

  public int AssignBook(Book book) {
    this.current_book = book;
    return 1;
  }

  public int FreeBook(Book book) {
    this.current_book = null;
    return 1;
  }
}

class Book {
  public String title;
  public String author;
  public String publisher;
  public int isAvail;

  public Book(String title, String author, String publisher, String isAvail) {
    this.title = title;
    this.author = author;
    this.publisher = publisher;
    this.isAvail = Integer.parseInt(isAvail);
  }

  public void Borrow() {
    this.isAvail = 0;
  }

  public void Return() {
    this.isAvail = 1;
  }

  public String toString(){
    return this.title;
  }
}

 class Library {
  public String name = "Old city Library";
  private ArrayList<Member> members;
  private ArrayList<Book> books;

  public void LoadBooks(String[][] books) {
    if (this.books == null) {
      this.books = new ArrayList<Book>();
    }
    for (String[] book : books) {
      this.books.add(new Book(book[0], book[1], book[2], book[3]));
    }
  }

  public void LoadMembers(String[][] members) {
    if (this.members == null) {
      this.members = new ArrayList<Member>();
    }
    for (String[] member : members) {
      this.members.add(new Member(member[0], member[1], member[2]));
      if (member[2] != "") {
        this.BorrowBook(member[0], member[2]);
      }
    }
  }

  public void ReturnBook(String member_name, String book_name) {
    //Update book
    Book book = this.GetBookByName(book_name);
    Member member = this.GetMemberByName(member_name);
    if (book != null && member != null) {
      book.Return();
      member.FreeBook(book);
    }
  }

  public void BorrowBook(String member_name, String book_name) {
    //Update book
    Book book = this.GetBookByName(book_name);
    Member member = this.GetMemberByName(member_name);
    if (book != null && member != null) {
      book.Borrow();
      member.AssignBook(book);
    }
  }

  public Member GetMemberByName(String member_name) {
    for (Member member : this.members) {
      if (member.name == member_name) {
        return member;
      }
    }
    return null;
  }

  public Book GetBookByName(String book_name) {
    for (Book book : this.books) {
      if (book.title == book_name) {
        return book;
      }
    }
    return null;
  }

  public ArrayList<Book> booksAvail() {
    ArrayList<Book> availBooks = new ArrayList<Book>();
    for (Book book : this.books) {
      if (book.isAvail == 1) {
        availBooks.add(book);
      }
    }
    return availBooks;
  }

  public ArrayList<Book> booksBorrowed() {
    ArrayList<Book> booksBorrowed = new ArrayList<Book>();
    for (Book book : this.books) {
      if (book.isAvail == 0) {
        booksBorrowed.add(book);
      }
    }
    return booksBorrowed;
  }

  public ArrayList<String> GetLibraryStatus() {
    ArrayList<String> status = new ArrayList<String>();
    for (Member member : this.members) {
      if (member.current_book != null) {
        status.add(member.name + " has borrowed " + member.current_book.title);
      }
    }
    return status;
  }
}